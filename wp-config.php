<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rnt' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'y-#/lz<~`S5M8f,i%Sr<@3Oe*3&-y[;5GrK?b(QE>/*Lg[C<&ub)jSd]fdM1)}A6' );
define( 'SECURE_AUTH_KEY',  ')_V_^2t3-7~5jd`r`0k&QC-DK!Ni3CKO {c1Y}U^L/0Q1TCZXs*P=4JvGQ(<?D#p' );
define( 'LOGGED_IN_KEY',    '~a+]6{5I@,>K/yyHwOGf?y|DbSO90Nf?43M<(fURRfVIn<Q/iIVpNa/mB1Lw2_UV' );
define( 'NONCE_KEY',        '&Yo4-1#uIhTqp|SF#d7@f4Kxwgq)x~X l|bqV8% e(Jn;Yg#>6ReDrd2,,Ws.4I`' );
define( 'AUTH_SALT',        'G6g_q1%I&uWh-9I{m%:@ZrO;+dg1Vd%M2>!2u{/eRbaLF5RZQ//o_^6]E?{q99,_' );
define( 'SECURE_AUTH_SALT', 'cx{?|3byh1>JSgXTMMUK,`tsocpQ.FV%E.S@$}0Gwp?lGITIVwb=%HT/W.nsfFJ ' );
define( 'LOGGED_IN_SALT',   '1>gX+-@+;zfn=?~Ey68lNIT6ushHD(}wuv!z0i+iP~ZT=|+k}oga~*Rhfy&>J[Y[' );
define( 'NONCE_SALT',       '9#6<{%N](U+i|Je)je*}coHr`-.qA#Im65g~-=/_>?bEuG8ttL]z;U/Gr$/u4V )' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rnt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
